
from tweepy import OAuthHandler
import tweepy
import my_config

ACCESS_TOKEN = my_config.ACCESS_TOKEN
ACCESS_SECRET = my_config.ACCESS_SECRET
CONSUMER_KEY = my_config.CONSUMER_KEY
CONSUMER_SECRET = my_config.CONSUMER_SECRET


def api_enrich(df):
    names = df.twitter
    num_tweet = list()
    num_following = list()
    num_followers = list()
    num_likes = list()

    auth = OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_SECRET)

    api = tweepy.API(auth)

    for name in names:
        if name == 'None':
            num_tweet.append(0)
            num_following.append(0)
            num_followers.append(0)
            num_likes.append(0)

        else:
            try:
                user = api.get_user(name)
                num_tweet.append(user.statuses_count)
                num_following.append(user.friends_count)
                num_followers.append(user.followers_count)
                num_likes.append(user.favourites_count)
            except:
                num_tweet.append(0)
                num_following.append(0)
                num_followers.append(0)
                num_likes.append(0)
    df['num_tweet'] = num_tweet
    df['num_following'] = num_following
    df['num_followers'] = num_followers
    df['num_likes'] = num_likes

    return df
