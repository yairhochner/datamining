import json
import requests
import pandas as pd
import logging

log_json = logging.getLogger('fund')
log_json.setLevel(logging.DEBUG)
log_json.addHandler(logging.FileHandler('fund.log'))


def json_scrape():
    """Scrape the recent funding banner on TechCrunch using json"""
    url = 'https://techcrunch.com/wp-json/tc/v1/crunchbase/recently-funded'
    page = requests.get(url)
    js = json.loads(page.content)

    cols = ['company_name', 'company_description', 'announce', 'raised', 'currency', 'city', 'region', 'country_code',
            'categories']
    df = pd.DataFrame(columns=cols)

    df['company_name'] = [x.get('company_name') for x in js]
    df['company_description'] = [x.get('company_short_description') for x in js]
    df['announce'] = [x.get('announced_on') for x in js]
    df['raised'] = [int(x.get('raised_amount', 0)) for x in js]
    df['currency'] = [x.get('raised_currency_code') for x in js]
    df['city'] = [y.get('city') if y is not None else 'None' for y in [x.get('location') for x in js]]
    df['region'] = [y.get('region') if y is not None else 'None' for y in [x.get('location') for x in js]]
    df['country_code'] = [y.get('country_code') if y is not None else 'None' for y in [x.get('location') for x in js]]
    cat = []
    for el in [x.get('categories') for x in js]:
        if el is None:
            cat.append('None')
        else:
            cat.append(','.join([y.get('name') if y is not None else 'None' for y in el]))
    df['categories'] = cat

    return df
