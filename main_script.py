import click
import my_config
from scraper import *
from db_operations import *
import logging
import api_twitter
from scrape_json import *


@click.command()
@click.option('--counter', default=None, type=int, help='Number of scraped articles')
@click.option('--display', is_flag=True, help='To display df on the screen')
@click.option('--show', '-s', multiple=True, help='Which column to display')
@click.option('--save', is_flag=True, help='To save in csv')
@click.option('--load', default=1, type=int, help='number of pages to load')
@click.option('--new', is_flag=True, help='Scrape only new entries without creating database')
def main(counter, display, show, save, load, new):
    logger = logging.getLogger('main')
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())

    # scrape and show data
    d = scrape(load_html(my_config.path, load), counter)
    df, df_author = df_def(d)
    df_fund = json_scrape()
    display_data(display, show, save, df)

    # Enrich
    df_author = api_twitter.api_enrich(df_author)
    logger.info('Enrich : OK')

    # creating the databases and inserting
    logger.info('Connecting to DB...')
    cnx = connect_todb(my_config)
    if new:
        insert_last(cnx, df, df_author, df_fund)
        logging.info('Insert recent: OK')
    else:
        logger.info('Creating and inserting in DB...')
        create_databases(cnx)
        logger.info('DB created')
        create_tables(cnx)
        logger.info('Table created')
        insert_data(cnx, df, df_author, df_fund)
        logger.info('Data insert: OK')
        insert_last(cnx, df, df_author)
        logging.info('Insert recent: OK')
    cnx.close()


if __name__ == '__main__':
    main()
