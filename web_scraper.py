import requests
from bs4 import BeautifulSoup
import pandas as pd
# import sys
import click
import xlwt
import sqlite3
import my_config
import sqlalchemy
import mysql
import mysql.connector
import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException


def select_all(conn):
    c = conn.cursor()

    c.execute('SELECT * FROM scrape_db')
    print(c.fetchall())


def create_databases(file):

    cnx = mysql.connector.connect(host='localhost', user='root', password=file.password)
    c = cnx.cursor()
    c.execute('create database if not exists scrape_db')

    return cnx, c


def to_sql(df, file):

    database_username = file.user
    database_password = file.password
    database_ip = 'localhost'
    database_port = '3306'
    database_name = 'scrape_db'
    engine = sqlalchemy.create_engine('mysql+mysqlconnector://{0}:{1}@{2}:{3}/{4}?charset=utf8mb4'.
                                                   format(database_username, database_password,
                                                          database_ip, database_port, database_name))
    df.to_sql(con=engine, name='table_scrape', if_exists='replace', index=False)

    cur = engine.connect()
    cur.execute('ALTER TABLE table_scrape ADD ID INT AUTO_INCREMENT PRIMARY KEY FIRST;')
    cur.close()


def web_scraper(counter, display, show, save):
    """ Scrape techcrunch.com and returns a dataframes. Receives the number of data points."""

    #define url
    url = 'https://techcrunch.com/'

    # open the load more button

    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--incognito")

    driver = webdriver.Chrome(options=chrome_options)
    driver.get(url)
    time.sleep(5)
    python_button = driver.find_element_by_class_name('load-more').click()
    # python_button.click()
    time.sleep(5)

    timeout = 5
    pages_to_scrape = 20
    for i in range(pages_to_scrape):
        driver.find_element_by_class_name('load-more').click()
        time.sleep(timeout)

    html = driver.page_source.encode('utf-8')

    # Main page scrap
    soup = BeautifulSoup(html, 'html.parser')
    # defining the data we scrap
    titles = list()
    times = list()
    authors = list()
    short_contents = list()
    links = list()

    # article scrapping
    list_post_bloc = soup.find_all(class_='post-block post-block--image post-block--unread')

    # define the limit if it exists
    if counter is not None:
        list_post_bloc = list_post_bloc[:counter]

    # scrapping
    for post_bloc in list_post_bloc:

        titles.append(post_bloc.find(class_='post-block__title__link').get_text(strip=True))
        times.append(post_bloc.find(class_='river-byline__time').get_text(strip=True))
        authors.append(post_bloc.find(class_='river-byline__authors').get_text(strip=True))
        short_contents.append(post_bloc.find(class_='post-block__content').get_text(strip=True))
        ref = post_bloc.find(class_='post-block__title__link').attrs['href']
        links.append(ref)

    # dictionary to define the data frame
    d = {'title': titles, 'short_content': short_contents, 'time': times, 'author': authors, 'link': links}

    # defining the data frame
    df = pd.DataFrame(d)

    if display:
        if save:
            df.to_excel('display_df_xls.xls')
        else:
            print(df)

    if show:
        if save:
            df[list(show)].to_excel('show_df_xls.xls')
        else:
            print(df[list(show)])

    # print(df['title'])
    return df


@click.command()
@click.option('--counter', default=None, type=int, help='Number of scraped articles')
@click.option('--display', is_flag=True, help='To display df on the screen')
@click.option('--show', '-s', multiple=True, help='Which column to display')
@click.option('--save', is_flag=True, help='To save in csv')

def main(counter, display, show, save):
    df = web_scraper(counter, display, show, save)

    # creating the databases

    cnx, c = create_databases(my_config)
    to_sql(df, my_config)

    cnx.close()
    c.close()

    # adding the df to the database


if __name__ == '__main__':
    main()