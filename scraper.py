from selenium import webdriver
import requests
from bs4 import BeautifulSoup
import pandas as pd
import time
import tqdm
import logging

log_scrape = logging.getLogger('scrape')
log_scrape.setLevel(logging.INFO)
log_scrape.addHandler(logging.StreamHandler())

log_data = logging.getLogger('scrape_data')
log_data.setLevel(logging.DEBUG)
log_data.addHandler(logging.FileHandler('scrape_data.log'))

log_shape = logging.getLogger('shapes')
log_shape.setLevel(logging.DEBUG)
log_shape.addHandler(logging.FileHandler('shapes.log'))


def load_html(path, nb_pages):
    """ Scrape techcrunch.com and returns dataframes. Receives the number of data points."""

    # define url
    url = 'https://techcrunch.com/'

    # open the load more button

    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--headless")

    driver = webdriver.Chrome(executable_path=path, chrome_options=chrome_options)
    driver.get(url)
    time.sleep(5)
    driver.find_element_by_class_name('load-more').click()
    time.sleep(5)

    timeout = 5

    for i in range(nb_pages):  # 20
        driver.find_element_by_class_name('load-more').click()
        time.sleep(timeout)

    html = driver.page_source.encode('utf-8')

    driver.close()
    log_scrape.info('Page loaded')
    return html


def scrape(html, counter=None):
    # Main page scrap
    soup = BeautifulSoup(html, 'html.parser')

    # defining the data we scrap
    titles = [x.get_text().strip() for x in soup.select('.post-block__title')][:counter]
    log_data.debug('\n'.join(titles))
    log_scrape.info('Titles: OK')
    times = [x['datetime'].replace('T', ' ').replace('Z', '') for x in soup.select('.river-byline time')][:counter]
    log_data.debug('\n'.join(times))
    log_scrape.info('Times: OK')
    authors = [x.find('a').get_text().strip() if x.find('a') else 'None' for x in
               soup.select('.river-byline__authors')][:counter]
    log_data.debug('\n'.join(authors))
    log_scrape.info('Authors: OK')
    short_contents = [x.get_text().strip() for x in soup.select('div .post-block__content')][:counter]
    log_data.debug('\n'.join(short_contents))
    log_scrape.info('Short content:OK')
    links = ['https://techcrunch.com' + x['href'] for x in soup.select('.post-block__title a')][:counter]
    log_data.debug('\n'.join(links))
    log_scrape.info('Links: OK')
    author_link = ['https://techcrunch.com' + x.find('a')['href'] if x.find('a') else 'None' for x in
                   soup.select('.river-byline__authors')][
                  :counter]
    log_data.debug('\n'.join(author_link))
    log_scrape.info('Author link: OK')

    twitter_list = list()
    log_scrape.info('Fetching twitter...')
    for ref in tqdm.tqdm(links):
        page_in = requests.get(ref)
        soup_in = BeautifulSoup(page_in.content, 'html.parser')
        tmp = soup_in.select_one('.article__byline__meta a')
        twitter_list.append([tmp.text if tmp else 'None'][0])
    log_data.debug(','.join(twitter_list))
    log_scrape.info('Twitter: OK')

    # description, num_tweet, num_following, num_followers, num_likes = api_twitter.api_enrich(twitter_list)
    # dictionary to define the data frame

    d = {'title': titles, 'short_content': short_contents, 'time': times, 'author': authors, 'link': links,
         'author_link': author_link, 'twitter': twitter_list}

    # 'author_description' : description, 'num_tweet': num_tweet,
    #      'num_following': num_following, 'num_followers': num_followers, 'num_likes': num_likes}

    log_shape.debug('\n'.join([x + ':' + str(len(d[x])) for x in d.keys()]))
    return d


def df_def(d):
    # defining the data frame
    df = pd.DataFrame(d)
    df_author = pd.DataFrame(columns=['name', 'twitter', 'link'])
    df_author.name = df.author.unique()
    df_author.twitter = [df.twitter[df.author == name].iloc[0] for name in df_author.name]
    df_author.link = [df.author_link[df.author == name].iloc[0] for name in df_author.name]
    return df, df_author


def display_data(display, show, save, df):
    """display function"""
    if display:
        if save:
            df.to_excel('display_df_xls.xls')
        else:
            print(df)

    if show:
        if save:
            df[list(show)].to_excel('show_df_xls.xls')
        else:
            print(df[list(show)])
