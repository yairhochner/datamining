import mysql
import mysql.connector
import dateutil
import sys
import logging

log_db = logging.getLogger('db')
log_db.setLevel(logging.DEBUG)
log_db.addHandler(logging.FileHandler('db.log'))


def connect_todb(file):
    cnx = mysql.connector.connect(host=file.host, user=file.user, password=file.password)
    return cnx


def create_databases(cnx):
    """Create db for """
    cur = cnx.cursor()
    cur.execute('drop database if exists scrape_db;')
    cur.execute('create database scrape_db character set utf8mb4 collate utf8mb4_unicode_ci;')
    cur.close()


def create_tables(cnx):
    """create tables"""
    cur = cnx.cursor()
    cur.execute("use scrape_db;")
    query1 = """create table table_title (title_id INT NOT NULL auto_increment,
                title VARCHAR(255), time DATETIME, short_content VARCHAR(255),
                link VARCHAR(255),
                PRIMARY KEY (title_id)
                );"""

    query2 = """create table table_author (author_id INT NOT NULL auto_increment,
                name VARCHAR(255),
                twitter VARCHAR(255),
                link VARCHAR(255),
                num_tweet INT,
                num_following INT,
                num_followers INT,
                num_likes INT,
                PRIMARY KEY (author_id)
                );
                """

    query3 = """create table author_article (author_id INT NOT NULL,
                title_id INT NOT NULL,
                FOREIGN KEY (author_id) REFERENCES table_author(author_id),
                FOREIGN KEY (title_id) REFERENCES table_title(title_id)
                );
                """
    query4 = """create table table_fund (company_id INT NOT NULL AUTO_INCREMENT,
                            company_name VARCHAR(255),
                            company_description VARCHAR(255),
                            announce DATETIME,
                            raised BIGINT,
                            currency VARCHAR(255),
                            city VARCHAR(255),
                            region VARCHAR(255),
                            country_code VARCHAR(255),
                            categories VARCHAR(255),
                            primary key(company_id)
                            );
                            """

    cur.execute(query1)
    cur.execute(query2)
    cur.execute(query3)
    cur.execute(query4)
    cur.close()


def insert_data(cnx, df, df_author, df_fund):
    """insert the scraped data"""
    cur = cnx.cursor()
    title_col = ['title', 'time', 'short_content', 'link']
    author_col = ['name', 'twitter', 'link', 'num_tweet', 'num_following', 'num_followers', 'num_likes']
    f_key_col = ['author', 'title']

    insert(cur, 'table_title', title_col, df)
    insert(cur, 'table_author', author_col, df_author)
    insert(cur, 'table_fund', df_fund.columns, df_fund)

    for row in df[f_key_col].itertuples(index=False):
        cur.execute("""insert into author_article ({}) values
                        ((select author_id from table_author where name={} limit 1),
                        (select title_id from table_title where title={} limit 1))
                        ;""".format('author_id, title_id', '%s', '%s'), list(row))
        log_db.debug('inserted :' + ','.join(list(row)))

    cnx.commit()
    cur.close()


def insert(cur, table, cols, df):
    insert_query = """INSERT INTO {} ({})
                            VALUES ({})
                        ;""".format(table, ', '.join(cols), ', '.join(['%s'] * len(cols)))
    for i, row in enumerate(df[cols].itertuples(index=False)):
        cur.execute(insert_query, list(row))


def insert_last(cnx, df, df_author, df_fund):
    cur = cnx.cursor()
    cur.execute('use scrape_db;')
    cur.execute('select max(time) from table_title;')
    last = cur.fetchall()[0][0]
    cur.execute('select max(announce) from table_fund;')
    last_fund = cur.fetchall()[0][0]

    # columns definition
    title_col = ['title', 'time', 'short_content', 'link']
    author_col = ['name', 'twitter', 'link', 'num_tweet', 'num_following', 'num_followers', 'num_likes']
    f_key_col = ['author', 'title']

    # insert to table title
    df = df[df.time.apply(lambda x: dateutil.parser.parse(x)) > last]
    if df.empty:
        print('no new entries')
        sys.exit(0)
    else:
        print('entered')
        insert(cur, 'table_title', title_col, df)

        # insert to table author
        cur.execute('select name from table_author;')
        authors = cur.fetchall()
        insert(cur, 'table_author', author_col, df_author[[not x for x in df_author.name.isin(authors)]])

        # insert to foreign key table
        for row in df[f_key_col].itertuples(index=False):
            cur.execute("""insert into author_article ({}) values
                        ((select author_id from table_author where name={} limit 1),
                        (select title_id from table_title where title={}))
                        ;""".format('author_id, title_id', '%s', '%s'), list(row))

        insert(cur, 'table_fund', df_fund.columns,
               df_fund[df_fund.announce.apply(lambda x: dateutil.parser.parse(x)) > last_fund])
        cnx.commit()
